// Selecting HTML elements. 
const articles = document.querySelector(".media-container")

// Defining URLs to fetch data from.
const urlSe = 'http://newsapi.org/v2/top-headlines?' +
    'country=se&' +
    'category=general&' +
    'apiKey=b9543fc989534de6a53e222525095158';

let reqSe = new Request(urlSe);

fetch(reqSe)
    .then((response) => {
        return response.json();
    })
    .then((data) => {
        console.log(data);

        for (i = 0; i < data.articles.length; i++) {
            let newDiv = document.createElement("div");
            newDiv.className = "article";
            let siteUrl = data.articles[i].url;
            newDiv.addEventListener('click', function () {
                window.open(`${siteUrl}`, '_blank');
            });
            newDiv.innerHTML = `<h4>${data.articles[i].title}</h4>`;
            if (data.articles[i].urlToImage == null) {
                newDiv.style.backgroundImage = `linear-gradient(rgba(0, 0, 0, 0.7),rgba(0, 0, 0, 0.3)),url(https://placekitten.com/200/300)`;
            } else {
                newDiv.style.backgroundImage = `linear-gradient(rgba(0, 0, 0, 0.7),rgba(0, 0, 0, 0.3)),url(${data.articles[i].urlToImage})`;
            };
            newDiv.style.backgroundSize = "cover";
            articles.append(newDiv);
        };
    });




