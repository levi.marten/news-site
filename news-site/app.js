// Selecting HTML elements. 
const elSwedenNews = document.querySelector(".Sweden-news");
const elBusiness = document.querySelector(".Business");
const elEntertainment = document.querySelector(".Entertainment");
const elTechnology = document.querySelector(".Technology");
const articles = document.querySelector(".media-container")

// Defining URLs to fetch data from.
const urlSe = 'http://newsapi.org/v2/top-headlines?' +
    'country=se&' +
    'category=general&' +
    'apiKey=9f915d2c6b6c41198fee9fa80d2682e8';

const urlBusiness = 'http://newsapi.org/v2/top-headlines?' +
    'category=business&' +
    'apiKey=9f915d2c6b6c41198fee9fa80d2682e8';

const urlEntertainment = 'http://newsapi.org/v2/top-headlines?' +
    'category=entertainment&' +
    'apiKey=9f915d2c6b6c41198fee9fa80d2682e8';

const urlTech = 'http://newsapi.org/v2/top-headlines?' +
    'category=technology&' +
    'apiKey=9f915d2c6b6c41198fee9fa80d2682e8';

let reqSe = new Request(urlSe);
let reqBusiness = new Request(urlBusiness);
let reqEntertainment = new Request(urlEntertainment);
let reqTech = new Request(urlTech);

// Eventlisteners for buttons/links
elSwedenNews.addEventListener('click', function () {
    fetch(reqSe)
    .then((response) => {
        return response.json();
    })
    .then((data) => {
        console.log(data);
    
        for (i = 0; i < data.articles.length; i++) {
            let newDiv = document.createElement("div");
            newDiv.className = `article`;
            newDiv.innerHTML = `<h4>${data.articles[i].title}</h4>`;
            newDiv.style.backgroundImage = `url(${data.articles[i].urlToImage})`;
            newDiv.style.backgroundSize = "cover";
            articles.append(newDiv);
        };
    });

});

elBusiness.addEventListener('click', function () {
    fetch(reqBusiness)
    .then((response) => {
        return response.json();
    })
    .then((data) => {
        console.log(data);
    
        for (i = 0; i < data.articles.length; i++) {
            let newDiv = document.createElement("div");
            newDiv.className = `article`;
            newDiv.innerHTML = `<h4>${data.articles[i].title}</h4>`;
            newDiv.style.backgroundImage = `url(${data.articles[i].urlToImage})`;
            newDiv.style.backgroundSize = "cover";
            articles.append(newDiv);
        };
    });
});

elEntertainment.addEventListener('click', function () {
    fetch(reqEntertainment)
    .then((response) => {
        return response.json();
    })
    .then((data) => {
        console.log(data);
    
        for (i = 0; i < data.articles.length; i++) {
            let newDiv = document.createElement("div");
            newDiv.className = `article`;
            newDiv.innerHTML = `<h4>${data.articles[i].title}</h4>`;
            newDiv.style.backgroundImage = `url(${data.articles[i].urlToImage})`;
            newDiv.style.backgroundSize = "cover";
            articles.append(newDiv);
        };
    });
});

// elTechnology.addEventListener('click', function () {
//     return let url = reqTech;
// });



// fetch(reqBusiness)
//     .then((response) => {
//         return response.json();
//     })
//     .then((data) => {
//         console.log(data);
//     });

// fetch(reqEntertainment)
//     .then((response) => {
//         return response.json();
//     })
//     .then((data) => {
//         console.log(data);
//     });

// fetch(reqTech)
//     .then((response) => {
//         return response.json();
//     })
//     .then((data) => {
//         console.log(data);
//     });